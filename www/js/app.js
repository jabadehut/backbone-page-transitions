(function () {
    console.log('start app');
    window.app = {
	Views: {},
	Extensions: {},
	Router: null,

    init: function () {
	console.log('app init');
	this.instance = new app.Views.App();
	console.log('starting Backbone.history.start()');
	Backbone.history.start();
    }
   };

    $(function() {
	console.log('empty function calling window.app.inti()');
	window.app.init();
});

    app.Router = Backbone.Router.extend({
        routes: {
                    'activity' : 'activity',
                    '': 'home'
                },

        home: function() {
                  console.log('home function in router');
                  var view = new app.Views.Home();
                  app.instance.goto(view);
		  console.log("after goto");
              },
        activity: function() {
                      console.log('activity function of router');
                      var view = new app.Views.Activity();
                      app.instance.goto(view);
		      console.log("after goto");

                  }

    });

    app.Extensions.View = Backbone.View.extend({

        initialize: function() {
          console.log('app.extensns.view.initialize()');
          this.router = new app.Router();
        },
        render: function(options) {
            console.log('render');
            options = options || {};

            if(options.page === true) {
                this.$el.addClass('page');
            }

            return this;
        },
        transitionIn: function(callback) {
              console.log('transitionIn');
              var view = this, 
			delay

              var transitionIn = function () {
                  view.$el.addClass('is-visible');
                  view.$el.on('transitionend', function () {
                      if(_.isFunction(callback)) {
			  console.log("had a callback function");
                          callback();
                      }
                  })
              };

              _.delay(transitionIn, 20);
          },
        transitionOut: function(callback){
           console.log('transitionOut');
           var view = this;
           view.$el.removeClass('is-visible');
           view.$el.on('transitioned', function () {
               if(_.isFunction(callback)) {
                   callback();
               }
           });
       }

    });
    app.Views.App = app.Extensions.View.extend({
        el: 'body',
        goto: function(view) {
          console.log('goto in app.Views.app');
          console.log( view);
          console.log(this.currentPage);
          var previous = this.currentPage || null;
          var next = view;

          if(previous) {
              console.log('executing as if there is a previous');
              previous.transitionOut(function() {
                  previous.remove();
              });
          }

          next.render({ page: true });
          this.$el.append(next.$el);
          next.transitionIn();
          this.currentPage = next;
          console.log("done with the goto");
      }
    });
    app.Views.Home = app.Extensions.View.extend({
        className: 'home',
        render: function() {
            console.log('rendering home    ');
            var template = _.template($('script[name=home]').html());
            console.log("got to html()");
            this.$el.html(template());
            return app.Extensions.View.prototype.render.apply(this, arguments);
        }
    });
    app.Views.Activity = app.Extensions.View.extend({
        className: 'activity',
        render: function() {
            console.log('rendering activity    ');
            var template = _.template($('script[name=activity]').html());
            console.log("got to html()");
            this.$el.html(template());
            return app.Extensions.View.prototype.render.apply(this, arguments);
        }
    });
}());
